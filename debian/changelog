lightyears (1.4-3) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 8 to 12.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 18:05:16 +0200

lightyears (1.4-2) unstable; urgency=medium

  * debian/patches/snd_disabled_typo.patch:
     - Move pygame.mixer to a place where pygame has already started up;
       it was unconditionally disabling sound. Thanks to Allan Wirth
       for diagnosing this (Closes: #742473).

 -- Siegfried-Angel Gevatter Pujals <rainct@ubuntu.com>  Sun, 21 Dec 2014 22:26:54 +0100

lightyears (1.4-1) unstable; urgency=low

  * New upstream version. Some of the changes are:
     - Added a Peaceful mode.
     - Added an in-game option to mute audio.
  * Update packaging to 3.0 (quilt) and dh7 and bump Standards-Version.
  * Drop python-pysco from Suggests, since upstream no longer recommends it.
  * Drop most patches since they have been incorporated upstream, update the
    remaining ones (doc_image_path.patch, remove_startup_checks.patch) and
    add snd_disabled_typo.patch and hardcode-code-dir.patch.
  * Update debian/copyright, debian/watch and debian/docs.
  * debian/lightyears.6:
     - Document the new --safe option.
  * debian/lightyears.desktop:
     - Add "StrategyGame" category.

 -- Siegfried-Angel Gevatter Pujals <rainct@ubuntu.com>  Fri, 22 Jul 2011 01:28:23 +0200

lightyears (1.3a-6) unstable; urgency=low

  [ Piotr Ożarowski ]
  * Suggest python-psyco on all architectures, as dpkg-gencontrol doesn't
    allow arch-specific dependency in architecture all packages anymore
    (Closes: 581306).
  * Bump Standards-Version to 3.8.4 (no changes needed).

 -- Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>  Tue, 18 May 2010 21:49:53 +0200

lightyears (1.3a-5) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control:
    - Switch Vcs-Browser field to viewsvn.

  [ Siegfried-Angel Gevatter Pujals ]
  * debian/control:
    - Bump Standards-Version to 3.8.2.
  * debian/patches/add_fullscreen_flag.patch:
    - Add a "--fullscreen" flag to start the game in fullscreen mode.
  * debian/lightyears.6:
    - Rework the manual page to improve the description of the game.
    - Document the "--window" and "--fullscreen" options.
    - Also document the available keyboard controls (Closes: #519077).

 -- Siegfried-Angel Gevatter Pujals <rainct@ubuntu.com>  Sat, 20 Jun 2009 20:33:50 +0200

lightyears (1.3a-4) unstable; urgency=low

  [ Siegfried-Angel Gevatter Pujals ]
  * debian/control:
    - Update the Homepage field to point to the new location.
    - Delete the comment from the Maintainer field.
    - Bump Standards-Version to 3.8.0.
    - Add python-psyco as a Suggests.
  * debian/copyright:
    - Change the download location to the new one.
  * debian/menu:
    - Change the formatting.
  * debian/patches/only_show_valid_resolutions.patch:
    - Only show resolutions supported by the system in the menu (LP: #219512).
  * debian/patches/fix_crash_in_sound_py.patch:
    - Avoid a crash which could happen when no sound channels were
      available; thanks to James Westby (LP: #194183).

  [ Sandro Tosi ]
  * debian/watch:
    - Fixed the watch file, as the download location changed.

  [ Piotr Ożarowski ]
  * debian/README.source file added

 -- Siegfried-Angel Gevatter Pujals <rainct@ubuntu.com>  Tue, 22 Jul 2008 15:04:20 +0200

lightyears (1.3a-3) unstable; urgency=low

  * debian/control:
     - replace ttf-bitstream-vera dependency with ttf-dejavu-core,
       as the first one is going to be removed (Closes: #461270).
     - change my e-mail address.
  * patches/font_usage.patch:
     - Change from Vera.ttf to DejaVuSans.ttf.

 -- Siegfried-Angel Gevatter Pujals (RainCT) <rainct@ubuntu.com>  Fri, 15 Feb 2008 21:03:34 +0100

lightyears (1.3a-2) unstable; urgency=low

  * patches/work_with_no_sound.patch:
     - make lightyears work on computers with no available
       audio device (Closes: #458286)

 -- Siegfried-Angel Gevatter Pujals (RainCT) <sgevatter@ubuntu.cat>  Sun, 30 Dec 2007 12:28:14 +0100

lightyears (1.3a-1) unstable; urgency=low

  * Initial release (Closes: #451421) (LP: #162635)
  * patches/start_windowed.patch:
     - start game in windowed mode and 800x600 resolution
  * patches/remove_shabang_intercept_py.patch:
     - remove unnecessary shabang from code/intercept.py.
  * patches/doc_image_path.patch:
     - fix .html documents to look for the images in the right path
  * patches/fix_icon_path.patch:
     - replace the default icon by that one used in the menu
  * patches/fix_manual_url.patch:
     - look for the web documentation in the right place
       (ie., /usr/share/doc/lightyears/html)
  * patches/font_usage.patch:
     - change the font path to use Vera.ttf from package
       ttf-bitstream-vera instead of a copy of the same
  * patches/remove_startup_checks.patch:
     - remove unnecessary versions checks from startup (that's what
       dpkg is for)

 -- Siegfried-Angel Gevatter Pujals (RainCT) <sgevatter@ubuntu.cat>  Sat, 17 Nov 2007 19:06:43 +0100
